package com.example.biodata2;

import com.google.firebase.database.IgnoreExtraProperties;

@IgnoreExtraProperties
public class Artist {
    private String artistId;
    private String artistName;
    private String artistNis;
    private String artistNoAbsen;
    private String artistEmail;
    private String artistGenre;
    private String artistKelas;

    public Artist() {
        //this constructor is required
    }

    public Artist(String artistId, String artistName, String artistNis, String artistNoAbsen, String artistEmail, String artistGenre, String artistKelas) {
        this.artistId = artistId;
        this.artistName = artistName;
        this.artistNis = artistNis;
        this.artistNoAbsen = artistNoAbsen;
        this.artistEmail = artistEmail;
        this.artistGenre = artistGenre;
        this.artistKelas = artistKelas;
    }

    public String getArtistId() {
        return artistId;
    }

    public String getArtistName() {
        return artistName;
    }

    public String getArtistNis() {
        return artistNis;
    }

    public String getArtistNoAbsen() {
        return artistNoAbsen;
    }

    public String getArtistEmail() {
        return artistEmail;
    }

    public String getArtistGenre() {
        return artistGenre;
    }

    public String getArtistKelas() {
        return artistKelas;
    }
}
